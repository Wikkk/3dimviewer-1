#===============================================================================
# $Id: LibraryMacros.txt 1283 2011-04-28 11:26:26Z spanel $
#
# 3DimViewer
# Lightweight 3D DICOM viewer.
#
# Copyright 2008-2012 3Dim Laboratory s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#===============================================================================

#-------------------------------------------------------------------------------
# Used macros

# Library name macro
macro( TRIDIM_LIBRARY _LIBRARY_NAME )
    set( TRIDIM_LIBRARY_NAME ${_LIBRARY_NAME} )
    set( TRIDIM_LIBRARY_PROJECT_NAME lib${_LIBRARY_NAME} )
    set( TRIDIM_LIBRARY_HEADERS "" )
    set( TRIDIM_LIBRARY_SOURCES "" )
    set( TRIDIM_PLUGIN_MOC_SOURCES "" )
endmacro( TRIDIM_LIBRARY )

# Add library source file
macro( ADD_LIBRARY_SOURCE_FILE )
    set( TRIDIM_LIBRARY_SOURCES ${TRIDIM_LIBRARY_SOURCES} ${ARGV})
endmacro( ADD_LIBRARY_SOURCE_FILE )

# Add header file to the library
macro( ADD_LIBRARY_HEADER_FILE )
    set( TRIDIM_LIBRARY_HEADERS ${TRIDIM_LIBRARY_HEADERS} ${ARGV})
endmacro( ADD_LIBRARY_HEADER_FILE )

# Add sources directory - adds all source files from the directory
macro( ADD_LIBRARY_SOURCE_DIRECTORY _DIR )
    file( GLOB_RECURSE _TRIDIM_LIBRARY_SOURCES ${_DIR}/*.c ${_DIR}/*.cpp ${_DIR}/*.cc )
    list( APPEND TRIDIM_LIBRARY_SOURCES ${_TRIDIM_LIBRARY_SOURCES} )
endmacro( ADD_LIBRARY_SOURCE_DIRECTORY )

# Add include directory - adds all headers from the directory
macro( ADD_LIBRARY_HEADER_DIRECTORY _DIR )
    file( GLOB_RECURSE _TRIDIM_LIBRARY_HEADERS ${_DIR}/*.h ${_DIR}/*.hxx ${_DIR}/*.hpp )
    list( APPEND TRIDIM_LIBRARY_HEADERS ${_TRIDIM_LIBRARY_HEADERS} )
endmacro( ADD_LIBRARY_HEADER_DIRECTORY )

# Add dependency
macro( ADD_LIBRARY_DEPENDENCY _LIB )
    target_link_libraries( ${TRIDIM_LIBRARY_NAME} ${_LIB} )
endmacro( ADD_LIBRARY_DEPENDENCY )

# Build macro
macro( TRIDIM_LIBRARY_BUILD )
    add_library( ${TRIDIM_LIBRARY_NAME} STATIC 
                 ${TRIDIM_LIBRARY_SOURCES} 
                 ${TRIDIM_LIBRARY_HEADERS}
                 )
    set_target_properties( ${TRIDIM_LIBRARY_NAME} PROPERTIES
                           PROJECT_LABEL ${TRIDIM_LIBRARY_PROJECT_NAME}
                           DEBUG_POSTFIX d
                           LINK_FLAGS "${TRIDIM_LINK_FLAGS}"
                           )
endmacro( TRIDIM_LIBRARY_BUILD )

# Build macro
macro( TRIDIM_DLL_LIBRARY_ADDQT )
  if (BUILD_WITH_QT5)
    QT5_WRAP_CPP( TRIDIM_LIBRARY_MOC_SOURCES ${TRIDIM_LIBRARY_HEADERS} )    
    set(_components
        Core
        Gui
        Network
        Widgets
        PrintSupport
        WebKit
        WebKitWidgets
        OpenGL
        LinguistTools
      )
    foreach(_component ${_components})
      #message( warning "${TARGET} lib ${TRIDIM_LIBRARY_NAME} qt5 module ${_component}" )
      #qt5_use_modules(${TRIDIM_LIBRARY_NAME} ${_component}) 
    endforeach()
  else (BUILD_WITH_QT5)
    QT4_WRAP_CPP( TRIDIM_LIBRARY_MOC_SOURCES ${TRIDIM_LIBRARY_HEADERS} )
  endif (BUILD_WITH_QT5)
endmacro( TRIDIM_DLL_LIBRARY_ADDQT )

macro( TRIDIM_DLL_LIBRARY_BUILD )
    add_library( ${TRIDIM_LIBRARY_NAME} SHARED 
                 ${TRIDIM_LIBRARY_SOURCES} 
                 ${TRIDIM_LIBRARY_HEADERS}
                 )
    set_target_properties( ${TRIDIM_LIBRARY_NAME} PROPERTIES    
                           RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_CURRENT_SOURCE_DIR}/bin/"
                           RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/bin/"
                           RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_CURRENT_SOURCE_DIR}/bin/"
                           RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_CURRENT_SOURCE_DIR}/bin/"
                           LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_CURRENT_SOURCE_DIR}/bin/"
                           LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/bin/"
                           LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_CURRENT_SOURCE_DIR}/bin/"
                           LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_CURRENT_SOURCE_DIR}/bin/" 
                           PROJECT_LABEL ${TRIDIM_LIBRARY_PROJECT_NAME}
                           DEBUG_POSTFIX d
                           LINK_FLAGS "${TRIDIM_LINK_FLAGS}"
                           )
endmacro( TRIDIM_DLL_LIBRARY_BUILD )

# Build macro
macro( TRIDIM_QT4_LIBRARY_BUILD )
    # process headers by MOC and generate list of resulting source files
    QT4_WRAP_CPP( TRIDIM_LIBRARY_MOC_SOURCES ${TRIDIM_LIBRARY_HEADERS} )

    add_library( ${TRIDIM_LIBRARY_NAME} STATIC 
                 ${TRIDIM_LIBRARY_SOURCES} 
                 ${TRIDIM_LIBRARY_HEADERS} 
                 ${TRIDIM_LIBRARY_MOC_SOURCES} 
                 )
    set_target_properties( ${TRIDIM_LIBRARY_NAME} PROPERTIES
                           PROJECT_LABEL ${TRIDIM_LIBRARY_PROJECT_NAME}
                           DEBUG_POSTFIX d
                           LINK_FLAGS "${TRIDIM_LINK_FLAGS}"
                           )
endmacro( TRIDIM_QT4_LIBRARY_BUILD )

# Build macro
macro( TRIDIM_QT5_LIBRARY_BUILD )
    # process headers by MOC and generate list of resulting source files
    QT5_WRAP_CPP( TRIDIM_LIBRARY_MOC_SOURCES ${TRIDIM_LIBRARY_HEADERS} )

    add_library( ${TRIDIM_LIBRARY_NAME} STATIC 
                 ${TRIDIM_LIBRARY_SOURCES} 
                 ${TRIDIM_LIBRARY_HEADERS} 
                 ${TRIDIM_LIBRARY_MOC_SOURCES} 
                 )
                 
    set(_components
        Core
        Gui
        Network
        Widgets
        PrintSupport
        WebKit
        WebKitWidgets
        OpenGL
        LinguistTools
      )
    foreach(_component ${_components})
      qt5_use_modules(${TRIDIM_LIBRARY_NAME} ${_component}) 
    endforeach()    
    
    set_target_properties( ${TRIDIM_LIBRARY_NAME} PROPERTIES
                           PROJECT_LABEL ${TRIDIM_LIBRARY_PROJECT_NAME}
                           DEBUG_POSTFIX d
                           LINK_FLAGS "${TRIDIM_LINK_FLAGS}"
                           )
endmacro( TRIDIM_QT5_LIBRARY_BUILD )

macro ( TRIDIM_QT_LIBRARY_BUILD )
  if (BUILD_WITH_QT5)
    TRIDIM_QT5_LIBRARY_BUILD()
  else (BUILD_WITH_QT5)
    TRIDIM_QT4_LIBRARY_BUILD()
  endif (BUILD_WITH_QT5)
endmacro ( TRIDIM_QT_LIBRARY_BUILD )

# Install macro
macro( TRIDIM_LIBRARY_INSTALL )
#  install( TARGETS ${TRIDIM_LIBRARY_NAME}
#           RUNTIME DESTINATION bin
#           LIBRARY DESTINATION lib
#           ARCHIVE DESTINATION lib
#  )
#  
#  export( TARGETS ${TRIDIM_LIBRARY_NAME}
#          FILE ${TRIDIM_LIBRARY_NAME}.cmake
#        )
#  foreach( HEADER ${TRIDIM_LIBRARY_HEADERS} )
#      file( RELATIVE_PATH FILE ${TRIDIM_SOURCE_DIR}/include ${HEADER} )
#      get_filename_component( DIR ${FILE} PATH )
#      install( FILES ${HEADER} DESTINATION include/${DIR} )
#  endforeach( HEADER )
endmacro( TRIDIM_LIBRARY_INSTALL )

# Install macro
macro( TRIDIM_DLL_LIBRARY_INSTALL )
  # do nothing
endmacro( TRIDIM_DLL_LIBRARY_INSTALL )